import { ProdutoService } from "../services/produto.service";

const produtoService = new ProdutoService();

// produto.controller
export class ProdutoController {

    constructor() { }

    postRegistarNovoProduto(req, res) {
        const payload = req.body; // extrair dados no corpo da requisição
        produtoService.inserir(payload);

        res.json({ messagem: 'Produto inserido com sucesso.', produto: payload });
    }

    getObterProdutoPorId(req, res) {        
        const id = parseInt(req.params['id']); // obter id nos parâmetros da url
        const produto = produtoService.obterProduto(id);

        res.json({ messagem: 'ok', produto: produto });
    }

    deleteRemoverProduto(req, res) {
        const id = parseInt(req.params['id']); // obter id nos parâmetros da url
        produtoService.remover(id);

        res.json({ messagem: `Produto (${id}) removido com sucesso.` });
    }

    putActualizarProduto(req, res) {
        const payload = req.body; // extrair dados no corpo da requisição
        produtoService.actualizar(payload);

        res.json({ messagem: `Produto (${payload.Id}) actualizado com sucesso.` });
    }

    getObterLitagemDeProdutos(req, res) {
        const produtos = produtoService.listarProdutos();

        res.json({ messagem: 'ok', produtos: produtos });
    }
}
