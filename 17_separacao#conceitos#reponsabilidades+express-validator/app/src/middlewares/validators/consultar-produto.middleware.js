import { validationResult } from 'express-validator/check';

export default (req, res, next) => {
    // regras de validação
    req.checkParams('id', 'Id inválido, aceita apenas valores numéricos.').isInt();
    
    const errors = req.validationErrors();
    if (errors) {
        res.status(400); // http status code  400 -> Bad Request
        res.send({ errors });
    } else {
        next();
    }
}
