import { ProdutoRepository } from '../repositories/produto.repository';

// produto.service
export class ProdutoService {

    constructor() {
        this.produtoRepository = new ProdutoRepository();
    }

    inserir(produto) {
        this.produtoRepository.inserir(produto);
    }

    obterProduto(id) {
        return this.produtoRepository.obterProduto(id);
    }

    remover(id) {
        this.produtoRepository.remover(id);
    }

    actualizar(produto) {
        this.produtoRepository.actualizar(produto);
    }

    listarProdutos() {
        return this.produtoRepository.listarProdutos();
    }
}
