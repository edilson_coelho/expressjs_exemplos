import { Produtos } from '../mocks/produto.mock';

let produtos = Produtos;

// produto.repository
export class ProdutoRepository {

    constructor() { }

    /**
     * Inserir novo produto
     * @param {*} produto 
     */
    inserir(produto) {
        produtos.push(produto);
    }

    /**
     * Pesquisar produto, passando como filtro o Id
     * @param {*} id 
     */
    obterProduto(id) {
        return produtos.find(l => l.Id === id) || null;
    }

    /**
     * Remover produto da lista de produtos
     * @param {*} id 
     */
    remover(id) {
        produtos = produtos.filter(l => l.Id !== id);
    }

    /**
     * Actualizar informações do produto
     * @param {*} produto 
     */
    actualizar(produto) {
        const _produto = this.obterProduto(produto.Id);

        _produto.Descricao = produto.Descricao;
        _produto.Quantidade = produto.Quantidade;
        _produto.Preco = produto.Preco;
    }

    /**
     * Pesquisar todos os produtos existentes na lista
     */
    listarProdutos() {
        return produtos;
    }
}
