// produto.model
export class Produto {

    /**
     * Atributos da class
     * @param {*} id 
     * @param {*} descricao 
     * @param {*} quantidade 
     * @param {*} preco 
     */
    constructor(id, descricao, quantidade, preco) {
        this.Id = id;
        this.Descricao = descricao;
        this.Quantidade = quantidade;
        this.Preco = preco;
    }
}
