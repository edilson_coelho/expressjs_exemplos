/*
*   importação dos módulos
*   Por padrão a função require('nome_do_module') como primeira instância olha para a pasta node_modules
*/
var express = require('express'); // importar módulo express

var PORT = 3000;
var app = express();

console.log('hello world'); // exibir resultado na consola

// Iniciar o servidor na porta 3000
app.listen(3000, function() {
    console.log('Servidor iniciado na porta ' + PORT);
});
