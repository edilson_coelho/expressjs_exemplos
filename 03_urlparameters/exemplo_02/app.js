var express = require('express'); // importar módulo express

var PORT = 3000;
var app = express();

var produtoRotas = express.Router();

// array de produtos
var listaProdutos = 
[
    { id: 1, nome: 'produto_01' },
    { id: 2, nome: 'produto_02' },
    { id: 3, nome: 'produto_03' }
];

produtoRotas.get('/produtos/:id/:nome', function(req, res) {
    // obter parametros na  URL
    var id = parseInt(req.params['id']); // string as number
    var nome = req.params['nome'];

    // filtrar produto da lista de produtos com base no identificador <ID> e <NOME> definido na rota
    var produto = listaProdutos.find(function(produto) { 
        return (produto.id === id && produto.nome.indexOf(nome) > -1); 
    });
    res.json(produto);
});

app.use('/api', produtoRotas);

// Iniciar o servidor na porta 3000
app.listen(3000, function() {
    console.log('Servidor iniciado na porta ' + PORT);
});
