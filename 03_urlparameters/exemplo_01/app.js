var express = require('express'); // importar módulo express

var PORT = 3000;
var app = express();

var produtoRotas = express.Router();

// array de produtos
var listaProdutos = 
[
    { id: 1, nome: 'produto_01' },
    { id: 2, nome: 'produto_02' },
    { id: 3, nome: 'produto_03' }
];

produtoRotas.get('/produtos', function(req, res) {
    res.json(listaProdutos);
});

produtoRotas.get('/produtos/:id', function(req, res) {
    // obter parametros na  URL
    var id = parseInt(req.params['id']); // conversão de string para inteiro, o parametro obtido é uma string

    // filtrar produto da lista de produtos com base no identificador <ID> definido na rota
    var produto = listaProdutos.find(function(produto) { return produto.id === id; });
    res.json(produto);
});

produtoRotas.delete('/produtos/:id', function(req, res) {
    // obter parametros na  URL
    var id = parseInt(req.params['id']);

    // remover o produto da lista de produtos passando como filtro o identificador <ID>
    listaProdutos = listaProdutos.filter(function(produto) { return produto.id !== id; });
    res.json(listaProdutos);
});

produtoRotas.post('/produtos/:id', function(req, res) {
    // obter parametros na  URL
    var id = parseInt(req.params['id']);

    var novoProduto = {
        id: id, 
        nome: 'produto_novo'
    };

    // adicionar o novo produto na lista de produtos
    listaProdutos.push(novoProduto);
    res.json(listaProdutos); // resultado, lista de produtos incluindo o novo produto adicionado
});

produtoRotas.put('/produtos/:id', function(req, res) {
    // obter parametros na  URL
    var id = parseInt(req.params['id']);

    var produto = listaProdutos.find(function(produto) { return produto.id === id; });
    produto.nome = 'produto_actualizado'; // modificação da propriedade 'nome'

    res.json(listaProdutos);
});

// registar rotas na aplicação
app.use('/api', produtoRotas);

// Iniciar o servidor na porta 3000
app.listen(3000, function() {
    console.log('Servidor iniciado na porta ' + PORT);
});
