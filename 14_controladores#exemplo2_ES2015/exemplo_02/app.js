import express from 'express'; // importar módulo express
import bodyParser from 'body-parser'; // importar módulo body-parser

const PORT = 3000;
const app = express();

const produtos =
    [
        { id: 1, nome: 'produto_01', quantidade: 3 },
        { id: 2, nome: 'produto_02', quantidade: 5 },
        { id: 3, nome: 'produto_03', quantidade: 14 }
    ];

// registar middlewares
app.use(bodyParser.json());
app.use(bodyParser({ urlencoded: true }));

const controller = {
    index: function(req, res) {
        res.send('index');
    },
    listaProdutos: (req, res) => {
        res.json(produtos);
    },
    obterProdutoPorId: (req, res) => {
        const id = parseInt(req.params.id); // obter valor definido como parametro da rota.
        const produto = produtos.find(produto => produto.id === id);

        res.json(produto);
    }
}

// registar rotas
app.get('/', controller.index);
app.get('/produtos', controller.listaProdutos);
app.get('/produtos/:id', controller.obterProdutoPorId);

// Iniciar o servidor na porta 3000
app.listen(PORT, () => {
    console.log(`Servidor iniciado na porta ${PORT}.`);
});
