var express = require('express'); // importar módulo express
var bodyParser = require('body-parser'); // importar módulo body-parser
var configuracoes = require('./settings'); // importar configurações do arquivo settings.json

var PORT = configuracoes.PORT; // obter o valor da porta no arquivo de configurações
var app = express();

var produtos =
    [
        { id: 1, nome: 'produto_01', quantidade: 3 },
        { id: 2, nome: 'produto_02', quantidade: 5 },
        { id: 3, nome: 'produto_03', quantidade: 14 }
    ];

// função responsável por realizar a validação de contrato
function validacaoContrato(produto) {
    var validacaoContratoErros = []; // adicionar os erros de validação num array

    if (!produto['id']) {
        validacaoContratoErros.push({ campo: 'id', erro: 'Não foi referenciado.' })
    }

    if (!produto['nome']) {
        validacaoContratoErros.push({ campo: 'nome', erro: 'Não foi referenciado.' });
    } else {
        if (produto['nome'].length < 10 || produto['nome'].length > 25) {
            validacaoContratoErros.push({ campo: 'nome', erro: 'Mínimo (10) caracteres e máximo (25).' });
        }
    }

    if (!produto['quantidade']) {
        validacaoContratoErros.push({ campo: 'quantidade', erro: 'Não foi referenciado.' });
    }

    return validacaoContratoErros;
}

// controlador -> Object Literal
var controller = {
    adicionarProduto: function (req, res) {

        var payload = req.body; // obter conteúdo no corpo da requisição
        var validacaoContratoErros = validacaoContrato(payload);

        if (validacaoContratoErros.length > 0) { // validação de contrato inválida
            res.status(500).json(validacaoContratoErros);
        } else {
            produtos.push(payload); // adicionar novo produto na lista
            res.status(200).json({ ok: true, mensagem: 'Novo produto adicionado.' }); // enviar resposta ao cliente
        }

    },
    listagemProdutos: function (res, res) {
        res.json(produtos);
    }
}

// registar middlewares
app.use(bodyParser.json());
app.use(bodyParser({ urlencoded: true }));

// registar rotas
app.get('/produtos', controller.listagemProdutos);
app.post('/produtos/novo', controller.adicionarProduto);

// Iniciar o servidor na porta 3000
app.listen(3000, function () {
    console.log('Servidor iniciado na porta ' + PORT);
});

