var express = require('express'); // importar módulo express
var bodyParser = require('body-parser'); // importar módulo body-parser
var configuracoes = require('./settings'); // importar configurações do arquivo settings.json

var PORT = configuracoes.PORT; // obter o valor da porta no arquivo de configurações
var app = express();

// controlador -> Object Literal
var controller = {
    index: function (req, res, next) {
        res.status(200).json(['TypeScript -> Super set...', 'ES5', 'ES6']);
    }
}

// registar middlewares
app.use(bodyParser.json());
app.use(bodyParser({ urlencoded: true }));

// registar rotas
app.get('/', controller.index);


// Iniciar o servidor na porta 3000
app.listen(3000, function () {
    console.log('Servidor iniciado na porta ' + PORT);
});
