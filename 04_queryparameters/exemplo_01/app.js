var express = require('express'); // importar módulo express

var PORT = 3000;
var app = express();

// registar rotas na aplicação

// URI -> http://localhost:3000/
app.get('/', function (req, res) {

    // No resultado especificamos o 'http status code' no primeiro parametro da função send(...)
    res.send(200, ['Node.js', 'ES6', 'Express.js', 'Babel', 'ES5']);
});

// URI -> http://localhost:3000/api/querystrings?ano=2018&mes=3
app.get('/api/querystrings', function (req, res) {
    
    // obter informações da URL
    var ano = req.query.ano;
    var mes = req.query['mes'];
    var dia = (req.query.dia === undefined) ? 1 : req.query.dia;

    res.status(200).send({ ano: ano, mes: mes, dia: dia});
});

// Iniciar o servidor na porta 3000
app.listen(3000, function() {
    console.log('Servidor iniciado na porta ' + PORT);
});
