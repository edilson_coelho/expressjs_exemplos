export const errorHandler = (error, req, res, next) => {
    res.status(error.status || 500);

    res.send('Ocorreu uma falha ao tentar satisfazer o pedido.');
}
