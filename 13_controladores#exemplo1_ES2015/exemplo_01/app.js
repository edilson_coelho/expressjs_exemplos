import express from 'express'; // importar módulo express
import bodyParser from 'body-parser'; // importar módulo body-parser

const PORT = 3000;
const app = express();

// registar middlewares
app.use(bodyParser.json());
app.use(bodyParser({ urlencoded: true }));

// controlador -> Object Literal
const controller = {
    index: (req, res) => {
        res.json(['Express.js', 'Node.js', 'ES6']);
    }
}

// registar rotas
app.get('/', controller.index);

// Iniciar o servidor na porta 3000
app.listen(PORT, () => {
    console.log(`Servidor iniciado na porta ${PORT}.`);
});
