import express from 'express'; // importar módulo express
import bodyParser from 'body-parser'; // importar módulo body-parser
import configuracoes from './settings'; // importar configurações do arquivo settings.json

const PORT = configuracoes.PORT; // obter o valor da porta no arquivo de configurações
const app = express();

// controlador -> Object Literal
const controller = {
    index: (req, res, next) => {
        res.status(200).json(['TypeScript -> Super set...', 'ES5', 'ES6']);
    }
}

// registar middlewares
app.use(bodyParser.json());
app.use(bodyParser({ urlencoded: true }));

// registar rotas
app.get('/', controller.index);

// Iniciar o servidor na porta 3000
app.listen(PORT, () => {
    console.log(`Servidor iniciado na porta ${PORT}.`);
});
