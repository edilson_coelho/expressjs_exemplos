var express = require('express');

var PORT = 3000;
var app = express();

// registar rotas na aplicação
// rota -> /
app.get('/', function(req, res) {        // URI http://localhost:3000/
    res.send('rotas, exemplo 02.');
});

// rota -> /lista
app.get('/lista', function(req, res) {   // URI http://localhost:3000/lista
    res.send(['express.js', 'node.js', 'es6']);
});

// rota -> /produto/livro
app.get('/produto/livro', function (req, res) { // URI http://localhost:3000/produto/livro
    res.send({ nome: 'criar serviços com node.js', ano: 2018 });
});

// Iniciar o servidor na porta 3000
app.listen(3000, function() {
    console.log('Servidor iniciado na porta ' + PORT);
});


