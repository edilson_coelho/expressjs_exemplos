var express = require('express');

var PORT = 3000;
var app = express();
var appRoutes = express.Router(); // a função Router() devolve um objecto do tipo Router

appRoutes.get('/', function(req, res, next) {
    res.json({ produtos: [ { nome: 'xpto', quantidade: 3, preco: 100 } ] });
});

appRoutes.get('/produto', function(req, res) {
    res.json({ nome: 'xpto', quantidade: 3, preco: 100 });
});

// registar rotas na aplicação
app.use('/api', appRoutes);

// Iniciar o servidor na porta 3000
app.listen(3000, function() {
    console.log('Servidor iniciado na porta ' + PORT);
});
