/*
*   importação dos módulos
*   Por padrão a função require('nome_do_module') como primeira instância olha para a pasta node_modules
*/
var express = require('express'); // importar módulo express

var PORT = 3000;
var app = express();

// registar rotas na aplicação
app.get('/', (req, res) => {
    res.send('rotas, exemplo 01.'); // enviar resposta ao cliente que realizou o pedido...
});

// Iniciar o servidor na porta 3000
app.listen(3000, function() {
    console.log('Servidor iniciado na porta ' + PORT);
});
