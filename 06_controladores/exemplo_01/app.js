var express = require('express'); // importar módulo express
var bodyParser = require('body-parser'); // importar módulo body-parser

var PORT = 3000;
var app = express();

// registar middlewares
app.use(bodyParser.json());
app.use(bodyParser({ urlencoded: true }));

// controlador -> Object Literal
var controller = {
    index: function(req, res) {
        res.json(['Express.js', 'Node.js', 'ES6']);
    }
}

// registar rotas
app.get('/', controller.index);

// Iniciar o servidor na porta 3000
app.listen(3000, function() {
    console.log('Servidor iniciado na porta ' + PORT);
});
