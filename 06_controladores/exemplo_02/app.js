var express = require('express'); // importar módulo express
var bodyParser = require('body-parser'); // importar módulo body-parser

var PORT = 3000;
var app = express();

var produtos = 
[
    { id: 1, nome: 'produto_01', quantidade: 3 },
    { id: 2, nome: 'produto_02', quantidade: 5 },
    { id: 3, nome: 'produto_03', quantidade: 14}
];

// registar middlewares
app.use(bodyParser.json());
app.use(bodyParser({ urlencoded: true }));

var controller = {
    index: function(req, res) {
        res.send('index');
    },
    listaProdutos: function(req, res) {
        res.json(produtos);
    },
    obterProdutoPorId: function(req, res) {
        var id = parseInt(req.params.id); // obter valor definido como parametro da rota.

         var produto = produtos.find(function(produto) {
             return produto.id === id; // true ou false
         });
         
         res.json(produto);
    }
}

// registar rotas
app.get('/', controller.index);
app.get('/produtos', controller.listaProdutos);
app.get('/produtos/:id', controller.obterProdutoPorId);

// Iniciar o servidor na porta 3000
app.listen(3000, function() {
    console.log('Servidor iniciado na porta ' + PORT);
});
