import express from 'express'; // importar módulo express

const PORT = 3000;
const app = express();

console.log('hello world'); // exibir resultado na consola

// Iniciar o servidor na porta 3000
app.listen(PORT, () => {
    console.log(`Servidor iniciado na porta ${PORT}.`);
});
