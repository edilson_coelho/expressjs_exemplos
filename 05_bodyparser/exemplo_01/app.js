var express = require('express'); // importar módulo express
var bodyParser = require('body-parser'); // importar módulo body-parser

var PORT = 3000;
var app = express();

app.use(bodyParser.json());
app.use(bodyParser({ urlencoded: true }));

var produtos = 
[
    { id: 1, nome: 'produto_01', quantidade: 3 },
    { id: 2, nome: 'produto_02', quantidade: 5 },
    { id: 3, nome: 'produto_03', quantidade: 14}
];

app.get('/', function(req, res) {
     res.json(produtos); // retorna a lista de produtos
});

app.post('/adicionar/produto', function(req, res) {
    var payload = req.body; // obter os valores enviados no corpo da requisição
    produtos.push(payload); // adicionar novo produto na lista

    res.json(produtos); // retorna a lista de produtos incluindo o registo adicionado
});

// Iniciar o servidor na porta 3000
app.listen(3000, function() {
    console.log('Servidor iniciado na porta ' + PORT);
});
