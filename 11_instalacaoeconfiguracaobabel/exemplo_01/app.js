import express from 'express'; // importar módulo express
import bodyParser from 'body-parser'; // importar módulo body-parser
import configuracoes from './settings'; // importar configurações do arquivo settings.json

const PORT = configuracoes.PORT; // obter o valor da porta no arquivo de configurações
const app = express();

// registar middlewares
app.use(bodyParser.json());
app.use(bodyParser({ urlencoded: true }));

// registar rotas
app.get('/', (req, res) => {
    res.status(200);
    res.send('Primeiro projecto feito com a sintaxe ES6.');
});

// Iniciar o servidor na porta 3000
app.listen(PORT, () => {
    console.log('Servidor iniciado na porta ' + PORT);
});

