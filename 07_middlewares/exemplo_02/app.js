var express = require('express'); // importar módulo express
var bodyParser = require('body-parser'); // importar módulo body-parser

var PORT = 3000;
var app = express();

var referenciaID = 'REF0001';

// controlador -> Object Literal
var controller = {
    index: function (req, res) {
        res.send('index');
    },
    registar: function (req, res) {
        var payload = req.body; // obter dados do corpo da solicitação ou pedido
        res.json({ resultado: payload }); // enviar como resposta o objecto payload
    }
}

/**
 * middleware -> validarTipoInformacaoEnviada
 * Verifica em todas as solicitações se o formato dos dados enviado correponde ao definido 'json'
 */
var validarTipoInformacaoEnviada = function (req, res, next) {
    // aceitar apenas json -> application/json
    if (req.headers['content-type'] !== 'application/json') {
        res.status(500).send('Formato incorrecto, aplicação suporta apenas dados no formato json.');
    } else {
        next();
    }
}

/**
 * middleware -> validarReferencia
 * Verificar se a referencia proveniente da URL corresponde a constante definida na aplicação
 */
var validarReferencia = function (req, res, next) {
    var queryParams = req.query; // obter parametros de query

    if (queryParams['referencia'] !== undefined) { // verificar se a referencia foi passada ou anexada a URL
        if (queryParams['referencia'] === referenciaID) {
            next();
        } else {
            res.status(500).send('Referencia invalida.');
        }
    } else {
        res.status(500).send('Referencia invalida.');
    }
}

// registar middlewares
app.use(bodyParser.json());
app.use(bodyParser({ urlencoded: true }));
app.use((validarReferencia)); // middleware customizado
app.use(validarTipoInformacaoEnviada); // middleware customizado

// registar rotas
app.get('/', controller.index);
app.post('/registar', controller.registar);

// Iniciar o servidor na porta 3000
app.listen(3000, function () {
    console.log('Servidor iniciado na porta ' + PORT);
});
