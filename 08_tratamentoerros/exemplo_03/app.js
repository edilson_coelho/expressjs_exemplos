var express = require('express'); // importar módulo express
var bodyParser = require('body-parser'); // importar módulo body-parser

var PORT = 3000;
var app = express();

// controlador -> Object Literal
var controller = {
    index: function (req, res, next) {
        next(new Error('Internal Server Error')); // forçar o erro "exception"
    }
}

var logsApp = function (error, req, res, next) {
    // aqui é aplicado a logica de registo de logs
    console.log('log registado.');
    next('erro'); // encaminha para próxima função de middleware para tratamento de erros
}
var errorhandler = function (error, req, res, next) {
    res.status(error.status || 500); // se o status for 'undefined' será atribuído o código 500.
    res.send('Ocorreu um erro no servidor.'); // retornar as mensagens erro para cliente.
}

// registar middlewares
app.use(bodyParser.json());
app.use(bodyParser({ urlencoded: true }));

// registar rotas
app.get('/', controller.index);

// middlewares de tratamento de erros
app.use(logsApp);
app.use(errorhandler);

// Iniciar o servidor na porta 3000
app.listen(3000, function () {
    console.log('Servidor iniciado na porta ' + PORT);
});
