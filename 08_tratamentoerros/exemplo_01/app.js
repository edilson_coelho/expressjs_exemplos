var express = require('express'); // importar módulo express
var bodyParser = require('body-parser'); // importar módulo body-parser

var PORT = 3000;
var app = express();

// controlador -> Object Literal
var controller = {
    index: function (req, res, next) {
         res.json(['Node.js', 'Express.js', 'ES6']);
    }
}

// registar middlewares
app.use(bodyParser.json());
app.use(bodyParser({ urlencoded: true }));

// registar rotas
app.get('/', controller.index);

app.use((error, req, res, next) => { // tratamento de erro padrão
    res.status(500).res.send('ocorreu um erro inesperado.');
 });

// Iniciar o servidor na porta 3000
app.listen(3000, function () {
    console.log('Servidor iniciado na porta ' + PORT);
});
