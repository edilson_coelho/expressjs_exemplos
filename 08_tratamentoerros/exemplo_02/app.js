var express = require('express'); // importar módulo express
var bodyParser = require('body-parser'); // importar módulo body-parser

var PORT = 3000;
var app = express();

// controlador -> Object Literal
var controller = {
    index: function (req, res, next) {
         next(new Error('Internal Server Error')); // forçar o erro "exception"
    }
}

// registar middlewares
app.use(bodyParser.json());
app.use(bodyParser({ urlencoded: true }));

// registar rotas
app.get('/', controller.index);

app.use((error, req, res, next) => { // tratamento de erro padrão
    res.status( error.status || 500 ); // se o status for 'undefined' será atribuído o código 500.
    res.send( error.message ); // retornar as mensagens erro para cliente.
 });

// Iniciar o servidor na porta 3000
app.listen(3000, function () {
    console.log('Servidor iniciado na porta ' + PORT);
});
